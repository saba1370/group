package com.citi.trading;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertThat;

import java.util.HashMap;

import org.hamcrest.core.IsEqual;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.InvestorTest.MockMarket;
import com.citi.trading.pricing.Pricing;
import com.citi.trading.pricing.PricingTest;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { BrokerServiceTest.Config.class })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BrokerServiceTest {

	@Configuration
	public static class Config {
		@Bean
		public BrokerService publishBrokerService() {
			return new BrokerService();
		}
		
		@Bean
		@Scope("prototype")
		public Investor publishInvestor() {
			return new Investor();
		}
		
		@Bean
		public Market publishMarket() {
			return Mockito.mock(Market.class);
		}
		
		@Bean
		public Pricing publishPricing() {
			return Mockito.mock(Pricing.class);
		}
	}

	@Autowired
	private BrokerService brokerService;


	@Test
	public void testGetAll() {
		int size = brokerService.getAll().size();
		assertThat(size, equalTo(3));
	}

	@Test
	public void testGetByID() {
		ResponseEntity<Investor> response = brokerService.getByID(0);
		assertThat(response.getBody().getCash(), equalTo(2000.0));
	}

	@Test
	public void testOpenAccount() {
		brokerService.openaccount(100);
		ResponseEntity<Investor> response = brokerService.getByID(4);
		assertThat(response.getBody().getCash(), equalTo(100.0));
	}

	@Test
	public void testRemoveAccount() {
		brokerService.closeAccount(3);
		assertThat(brokerService.getByID(3).getBody(), equalTo(null));
	}
	
}
