package com.citi.trading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts")
public class BrokerService {

	private Map<Integer, Investor> investors = new HashMap<Integer, Investor>();
	private static int newID = 4;

	@Autowired
	private ApplicationContext context;

	@PostConstruct
	private void init() {

		Map<String, Integer> portfolio1 = new HashMap<>();
		portfolio1.put("MSFT", 10000);
		Investor investor1 = context.getBean(Investor.class);
		investor1.setCash(2000);
		investor1.setPortfolio(portfolio1);
		investor1.setID(0);
		investors.put(0, investor1);

		Map<String, Integer> portfolio2 = new HashMap<>();
		portfolio2.put("MSFT", 100);
		portfolio2.put("AAPL", 100);
		Investor investor2 = context.getBean(Investor.class);
		investor2.setCash(20000);
		investor2.setPortfolio(portfolio2);
		investor2.setID(1);
		investors.put(1, investor2);

		Map<String, Integer> portfolio3 = new HashMap<>();
		portfolio3.put("MSFT", 1000);
		portfolio3.put("AAPL", 1000);
		Investor investor3 = context.getBean(Investor.class);
		investor3.setCash(8000);
		investor3.setPortfolio(portfolio3);
		investor3.setID(2);
		investors.put(2, investor3);
	}
	
	@GetMapping(value="", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Investor> getAll() {
		List<Investor> investorsList = new ArrayList<Investor>();
		for (int id : investors.keySet()) {
			investorsList.add(investors.get(id));
		}
		return investorsList;
	}

	@RequestMapping(value="/{id}",method=RequestMethod.GET,
			 produces={MediaType.APPLICATION_JSON_UTF8_VALUE})
	public  ResponseEntity<Investor> getByID(@PathVariable("id") int ID) {
		if(investors.containsKey(ID)) {
			return new ResponseEntity<>(investors.get(ID), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

    @RequestMapping(value="/cash/{cash}", method=RequestMethod.POST)
	public Investor openaccount(@PathVariable("cash") double cash) {

		Investor investor = context.getBean(Investor.class);
		investor.setCash(cash);
		investor.setPortfolio(new HashMap<String, Integer>());
		investor.setID(newID);
		investors.put(newID, investor);
		newID++;
		return investor;
	}

	@RequestMapping(value="/del/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<Investor> closeAccount(@PathVariable("id") int ID) {
		//investors.remove(ID);
		if(investors.containsKey(ID)) {
			investors.remove(ID);
			return new ResponseEntity<>(null, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
}
